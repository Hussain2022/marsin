// __dirname stands for diractory name which contains the path to the directory the current script lives in
// __filename which contains the complete path from the root of the hard drive all the way to the src folder
// For more info go to https://nodejs.org/dist/latest-v13.x/docs/api/path.html
// npmjs.com/package/handlebars
// expressjs.com/api.html

const path = require('path')
const express = require('express')
const hbs = require('hbs')

const app = express()

// Define paths for Express config
const publicDirPath = path.join(__dirname, '../public')
const viewsPath = path.join(__dirname, '../templates/views')
const partialsPath = path.join(__dirname, '../templates/partials')

// Setup handlebars engine and views location
app.set('view engine', 'hbs')
app.set('views', viewsPath)
hbs.registerPartials(partialsPath)

// Setup static directory to serve
app.use(express.static(path.join(publicDirPath)))

app.get('/', (req, res) => {
    res.render('index', {
        title: 'MarsIN',
        name: 'Hussain Alhashim'
    })
})


app.get('/about', (req, res) => {
    res.render('about', {
        title: 'MarsIN',
        name: 'Hussain Alhashim'
    })
})

app.get('/help', (req, res) => {
    res.render('help', {
        title: 'MarsIN help page',
        message: 'Need help? Contact us at ',
        link: 'marsin.com',
        name: 'Hussain Alhashim'
    })
})

app.get('/help/*', (req, res) => {
    res.render('404', {
        title: '404',
        name: 'Hussain Alhashim',
        message: 'Help article not found'
    })
})

app.get('*', (req, res) => {
    res.render('404', {
        title: '404',
        name: 'Hussain Alhashim',
        message: 'This page doesn\'t exit'
    })
})

app.listen(3000, () => {
    console.log('Server is up on port 3000.')
})